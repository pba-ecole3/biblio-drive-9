<?php
session_start();
if (isset($_SESSION['Profil'])) {
    if ($_SESSION['Profil'] == 'admin'){
      header('Location: accueilAdmin.php');
      exit();
    }
  }  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "templates/header.php"; ?>
</head>
<style>
    .BoutonDeconnexion {

        text-align: center;
        color: #fff;
        /* couleur du texte */
        background-color: #06283d;
        /* couleur du fond */
        border-color: #ffffff;
        /* couleur de la bordure */
        border-radius: 10px;
        padding: 10px;
        text-decoration: none;
        /* Supprime le soulignement */
        color: #fff;
        /* Couleur du texte */
    }

    .BoutonDeconnexion:hover {
        background-color: orange;
        border-color: #06283d;
        color: #fff;
    }

    body {
        text-align: center;
        background-color: #caad6e;
        color: white;
        overflow-x: hidden;
    }

    .pasconnecte {
        color: red;
    }

    .container {
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 20px;
        margin-top: 20px;
    }

    .imagelivre {
        max-width: 300px;
        height: auto;
        float: left;
    }

    .content {
        text-align: justify;
        color: #fff;
        display: flex;
        flex-direction: column;
        align-items: center;
        border: 1px solid #ffffff;
        padding: 2rem;
    }

    h5 {
        text-align: left;
    }

    .titrelivre {
        font-size: 24px;
        margin-top: 0;
    }

    .nomauteur {
        margin-bottom: 10px;

    }

    .resumelivre {
        font-size: medium;
        margin-top: 20px;
    }

    .btnReserverD {
        border-color: #ffffff;
        border-radius: 25px;
        text-align: center;
        color: #fff;
        justify-content: space-between;
        padding: 15px;
        background-color: green;
    }

    .indispo {
        color: red;
    }

    .btnReserverD:hover {
        background-color: grey;
    }

    .DetailsLivre {
        text-align: center;
        color: #fff;
        /* couleur du texte */
        background-color: #06283d;
        /* couleur du fond */
        border-color: #ffffff;
        /* couleur de la bordure */
        border-radius: 10px;
        padding: 10px;
        text-decoration: none;
        /* Supprime le soulignement */
        color: #fff;
        /* Couleur du texte */
    }

    .DetailsLivre:hover {
        background-color: orange;
        border-color: #06283d;
        color: #fff;
    }
    .nbrPanier{
        position: absolute;
        top: 35px;
        right: 740px;
        background-color: orange;
        height: 20px;
        width: 20px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        font-size: 12px;
    }
</style>


<div class="row">
    <div class="col-md-8 ">
        <?php
        require_once("config/connexion.php");

        $nolivre = $_GET['nolivre'];

        $selectLivre = $connexion->prepare(
            "SELECT livre.*
            FROM livre 
            WHERE nolivre = :nolivre;"
        );
        $selectLivre->bindParam(':nolivre', $nolivre);
        $selectLivre->execute();

        $dispo = $connexion->prepare(
            "SELECT *
        FROM emprunter 
        WHERE nolivre = :nolivre AND dateretour IS NULL;"
        );

        $dispo->bindParam(':nolivre', $nolivre);
        $dispo->execute();

        $livreIndisponible = $dispo->fetch(PDO::FETCH_OBJ);
        $rechercheAuteur = $connexion->prepare(
            "SELECT auteur.nom AS nom_auteur
            FROM livre
            JOIN auteur ON livre.noauteur = auteur.noauteur
            WHERE livre.nolivre = :nolivre;"
        );
        $rechercheAuteur->bindParam(':nolivre', $nolivre);
        $rechercheAuteur->execute();
        $auteur = $rechercheAuteur->fetch(PDO::FETCH_OBJ);

        while ($unLivre = $selectLivre->fetch(PDO::FETCH_OBJ)) {
            // echo json_encode($unLivre);
            echo "<div class='container'>";
            echo "<img class='imagelivre' src='" . $unLivre->image . "' alt='Image du livre'>";
            echo "<div class='content'>";
            echo "<p class='titre'>Titre : " . $unLivre->titre . "</p>";
            echo "<p class='nomauteur'>Auteur : " . $auteur->nom_auteur . "</p>";
            echo "<p class='isbn13'>ISBN13 : " . $unLivre->isbn13 . "</p>";
            echo "<h2 class='resumelivre'>Résumé du livre : <br>" . $unLivre->resume . "</h2>";


            echo "</div>";
            if (isset($_SESSION['Mel'])) {
                if ($livreIndisponible) {
                    echo "<p class ='indispo'> Ce livre est indisponible </p>";
                    echo "</div>";
                } else {
                    
                    if (isset($_SESSION['panier'][$nolivre])) {
                        echo "Le livre est déjà dans le panier";
                    } else {
                        if (count($_SESSION['panier']) + $_SESSION['NbrLivresEmpruntes'] >= 5) {
                            echo "Vous ne pouvez pas emprunter plus de 5 livres.";
                        } else {
                            echo "<form action=panier.php?nolivre=" . $unLivre->nolivre . "' method='POST'>";
                            echo "<button type='submit' class='btnReserverD'>Reserver</button>";
                            $_SESSION['panier'][$nolivre] = 1;
                        }
                    }
                    
                    echo "</div>";
                    echo "</form>"; 
                }   
            } else {
                echo "<p class='pasconnecte'> Vous devez être connecté pour réserver ce livre </p>";
                echo "</div>";
            }
        }
        ?>
    </div>
    <?php

    if (isset($_SESSION['Mel'])) {
    ?>
        <div class="col-md-4 ">
            <h5>Connecté en tant que :</h5>
            <div class='form-group'>
                <p>Mail: <?php echo $_SESSION['Mel']; ?></p>
                <p>Nom: <?php echo $_SESSION['Nom']; ?></p>
                <p>Prénom: <?php echo $_SESSION['Prenom']; ?></p>
                <p>Adresse: <?php echo $_SESSION['Adresse']; ?></p>
                <p>Ville: <?php echo $_SESSION['Ville']; ?></p>
                <p>Code Postal: <?php echo $_SESSION['Codepostal']; ?></p>
                <p>Vous avez <?php echo $_SESSION['NbrLivresEmpruntes']; ?> emprunt(s) en cours</p>
                <a href="utils/deconnexion.php" class="BoutonDeconnexion">
                    Déconnexion
                </a>
            </div>

        </div>
    <?php
    } else {
    ?>
        <div class="col-md-4 ">
            <form action='identification.php' method='POST'>
                <div class='form-group'>
                    <label for='Mel'>Identifiant</label>
                    <input type='text' class='form-control' id='Mel' name='Mel' required><br>
                    <label for='MotDePasse'>Mot de Passe</label>
                    <input type='password' class='form-control' id='MotDePasse' name='MotDePasse' required><br>
                    <button type='submit' class='btn btn-custom'>Se Connecter</button>
                </div>
            </form>
        </div>
</div>
<?php
    }
    include "templates/footer.php";
?>