<?php
session_start();
require_once("config/connexion.php");
?>
<!DOCTYPE html>
<html lang="en">
<style>
    body {
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        height: 100vh;
        margin: 0;
    }

    textarea#resume {
        width: 300px;
    }

    .AjoutLivre {
        color: red;
        border: none;
        padding: 1rem;
        display: flex;
        flex-direction: column;
        align-items: center;
    }
</style>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "templates/headeradmin.php"; ?>

</head>

<body>
    <form action='AddLivre.php' method='POST'>
        <div class='AjoutLivre'>
            <h2>Ajouter un livre</h2>
            <div class="contenuajout">
                <label for="auteur"> Auteur :</label>
                <select id="auteur" name="auteur" required>
                    <?php
                    $query = "SELECT * FROM auteur";
                    $result = $connexion->query($query);
                    while ($cherche = $result->fetch(PDO::FETCH_ASSOC)) {
                        echo "<option value='" . $cherche['noauteur'] . "'>" . $cherche['nom'] . "</option>";
                    }
                    ?>
                </select><br><br>

                <label for="titre"> Titre :</label>
                <input type='text' id='titre' name='titre' required><br><br>

                <label for="ISBN13"> ISBN13 :</label>
                <input type='text' id='ISBN13' name='ISBN13' required><br><br>

                <label for="AnneedeParution"> Annee de Parution :</label>
                <input type='text' id='AnneedeParution' name='AnneedeParution' required><br><br>

                <label for="resume" class="resume"> Resume :</label>
                <textarea type='text' id='resume' name='resume' required></textarea><br><br>

                <label for="imageAdd"> Image :</label>
                <input type='text' id='imageAdd' name='imageAdd' required><br><br>
                <button type='submit' class='btn btn-custom'>Valider</button>
            </div>
        </div>
    </form>
</body>