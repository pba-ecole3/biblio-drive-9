<?php
require_once("config/connexion.php");
session_start();


$meluser = $_POST["meluser"];
$mdpUser = $_POST["mdpUser"];
$nom = $_POST["nom"];
$prenom = $_POST["prenom"];
$adresse = $_POST["adresse"];
$ville = $_POST["ville"];
$codePostal = $_POST["codePostal"];

$requete = "INSERT INTO utilisateur (mel, motDePasse, nom, prenom, adresse, ville, codePostal, profil) VALUES ( :meluser, :mdpUser, :nom, :prenom, :adresse, :ville, :codePostal, 'user')";

$select = $connexion->prepare($requete);


$select->bindParam(':meluser', $meluser);
$select->bindParam(':mdpUser', $mdpUser);
$select->bindParam(':nom', $nom);
$select->bindParam(':prenom', $prenom);
$select->bindParam(':adresse', $adresse);
$select->bindParam(':ville', $ville);
$select->bindParam(':codePostal', $codePostal);

if ($select->execute()) {
    header('Location: accueilAdmin.php');
    exit;
} else {
    echo "Une erreur s'est produite lors de l'exécution de la requête.";
}
