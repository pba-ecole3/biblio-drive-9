<?php
session_start();
require_once("config/connexion.php");

// Sélectionner les informations d'un livre
$requeteLivre = "SELECT * FROM livre WHERE nolivre = :nolivre";
$selectLivre = $connexion->prepare($requeteLivre);

// Traite les livres dans le panier
foreach ($_SESSION['panier'] as $nolivre => $quantite) {
    // Exécute la requête pour récupérer les informations du livre
    $selectLivre->bindParam(':nolivre', $nolivre);
    $selectLivre->execute();

    while ($unLivre = $selectLivre->fetch(PDO::FETCH_OBJ)) {
        $dateEmprunt = date('Y-m-d'); // Obtient la date du jour

        // Requête pour insérer les données d'emprunt dans la table emprunter
        $requete = "INSERT INTO emprunter (mel, nolivre, dateemprunt, dateretour) VALUES (:mel, :nolivre, :dateemprunt, NULL)";

        // Préparation de la requête
        $insert = $connexion->prepare($requete);

        // Liaison des paramètres
        $insert->bindParam(':mel', $_SESSION["Mel"]);
        $insert->bindParam(':nolivre', $nolivre);
        $insert->bindParam(':dateemprunt', $dateEmprunt);

        // Exécution de la requête d'insertion
        $insert->execute();
        $_SESSION['NbrLivresEmpruntes'] = $_SESSION['NbrLivresEmpruntes'] + 1;
    }
}


$_SESSION['panier'] = [];

header('Location: panier.php');
exit();
