<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "templates/header.php"; ?>
</head>

<?php

// On se connecte
require_once("config/connexion.php");
$mel = $_POST["Mel"];
$motDePasse = $_POST["MotDePasse"];
$requete = "SELECT * FROM utilisateur WHERE mel='" . $mel . "' AND motDePasse = '" . $motDePasse . "'";
// Envoi de la requête vers MySQL
$select = $connexion->query($requete);
// Les résultats retournés par la requête seront traités en mode objet
$select->setFetchMode(PDO::FETCH_OBJ);
$enregistrement = $select->fetch();
if ($enregistrement) { // si enregistrement OK = on est connecté

    $_SESSION["Mel"] = $enregistrement->mel;
    $_SESSION["Nom"] = $enregistrement->nom;
    $_SESSION["Prenom"] = $enregistrement->prenom;
    $_SESSION["Adresse"] = $enregistrement->adresse;
    $_SESSION["Ville"] = $enregistrement->ville;
    $_SESSION["Codepostal"] = $enregistrement->codepostal;
    $_SESSION["Profil"] = $enregistrement->profil;
    $_SESSION['panier'] = [];

    $nbrLivre = $connexion->prepare(
        "SELECT COUNT(*) AS CompteLivre
    FROM emprunter 
    WHERE mel = :mel AND dateretour IS NULL;"
    );

    $nbrLivre->bindParam(':mel', $mel);
    $nbrLivre->execute();
    $nbrLivreResult=$nbrLivre->fetch(PDO::FETCH_ASSOC);

    $CompteLivre = $nbrLivreResult['CompteLivre'];

    $_SESSION['NbrLivresEmpruntes'] = $CompteLivre;

} else {
}


if ($_SESSION['Profil'] == 'admin'){
  header('Location: accueilAdmin.php');
  exit();
}


header('Location: profil.php');
exit();
?>