<?php
session_start();
if (isset($_SESSION['Profil'])) {
    if ($_SESSION['Profil'] == 'admin'){
      header('Location: accueilAdmin.php');
      exit();
    }
  }  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "templates/header.php"; ?>

    <style>
        body {
            text-align: center;
            background-color: #caad6e;
            color: white;
        }

        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 20px;
            margin-top: 20px;
        }

        .imagelivre {
            max-width: 300px;
            height: auto;
            float: left;
        }

        .content {
            text-align: justify;
            color: #fff;
            display: flex;
            flex-direction: column;
            align-items: center;
            border: 1px solid #ffffff;
            padding: 2rem;
        }

        .titrelivre {
            font-size: 24px;
            margin-top: 0;
        }

        .nomauteur {
            margin-bottom: 10px;

        }

        .resumelivre {
            margin-top: 20px;
        }

        .DetailsLivre {
            text-align: center;
            color: #fff;
            /* couleur du texte */
            background-color: #06283d;
            /* couleur du fond */
            border-color: #ffffff;
            /* couleur de la bordure */
            border-radius: 10px;
            padding: 10px;
            text-decoration: none;
            /* Supprime le soulignement */
            color: #fff;
            /* Couleur du texte */
        }

        .DetailsLivre:hover {
            background-color: orange;
            border-color: #06283d;
            color: #fff;
        }
    </style>
</head>

<body>
    <?php
    require_once("config/connexion.php");

    if (isset($_GET['nomAuteur'])) {
        $nomAuteur = $_GET['nomAuteur'];
        $_SESSION["nomauteur"] = $nomAuteur;
        $selectLivre = $connexion->prepare(
            "SELECT livre.*
            FROM livre 
            INNER JOIN auteur ON livre.noauteur = auteur.noauteur
            WHERE auteur.nom = :nomAuteur;"
        );
        $selectLivre->bindParam(':nomAuteur', $nomAuteur);
        $selectLivre->execute();

        while ($unLivre = $selectLivre->fetch(PDO::FETCH_OBJ)) {
            echo "<div class='container'>";
            echo "<img class='imagelivre' src='" . $unLivre->image . "' alt='Image du livre'>";
            echo "<div class='content'>";
            echo "<h2 class='titrelivre'>" . $unLivre->titre . "</h2>";
            echo "<p class='nomauteur'>Par " . $nomAuteur . "</p>";
            echo "<p class='resumelivre'>" . $unLivre->resume . "</p>";
            echo "</div>";
            echo "<a href='livredetail.php?nolivre=" . $unLivre->nolivre . "' ><i class='fa-solid fa-circle-info fa-2xl fa-spin'></i></a>";
            echo "</div>";
        }
    } else {
        echo "Nom de l'auteur Inconnu";
    }
    ?>
</body>
<?php include "templates/footer.php"; ?>

</html>