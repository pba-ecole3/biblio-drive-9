<?php
session_start();
if (isset($_SESSION['Profil'])) {
    if ($_SESSION['Profil'] == 'admin'){
      header('Location: accueilAdmin.php');
      exit();
    }
}  
require_once("config/connexion.php");
$nolivre = $_GET['nolivre'];

$selectLivre = $connexion->prepare(
    "SELECT livre.*
    FROM livre 
    WHERE nolivre = :nolivre;"
);
$selectLivre->bindParam(':nolivre', $nolivre);
$selectLivre->execute();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "templates/header.php"; ?>
</head>
<style>
    body {
        text-align: center;
    }
    h5 {
        text-align: left;
    }

    .BoutonDeconnexion {

        text-align: center;
        color: #fff;
        /* couleur du texte */
        background-color: #06283d;
        /* couleur du fond */
        border-color: #ffffff;
        /* couleur de la bordure */
        border-radius: 10px;
        padding: 10px;
        text-decoration: none;
        /* Supprime le soulignement */
        color: #fff;
        /* Couleur du texte */
    }

    .BoutonDeconnexion:hover {
        background-color: orange;
        border-color: #06283d;
        color: #fff;
    }
    .listeLivre{
        color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 0.9rem;
    }
    .boutonSuprr{
        text-decoration: none;
        border-radius: 10px;
        text-align: center;
        justify-content: center;
        color: #fff;
        padding: 0.3rem;
    }
    .btnValider{
        border-radius: 25px;
        text-align: center;
        color: #fff;
        justify-content: center;
        padding: 0.3rem;
        background-color: green;
    }

    .btnValider:hover {
        background-color: greenyellow;
    }
</style>

<body>
    <div class="row">
        <div class="col-md-8 ">
            <?php

            if (!isset($_SESSION['Mel'])) {
                echo "<p class='pasconnecte'> Vous devez être connecté pour accéder à votre panier </p>";
            } else {
                echo "<h2>Panier</h2>";
                if (isset($_SESSION['panier'])) {
                    if (count($_SESSION['panier']) <= 0) {
                        echo"Votre Panier est vide";
                    }else{
                    foreach ($_SESSION['panier'] as $nolivre => $quantite) {
                        $selectLivre->bindParam(':nolivre', $nolivre);
                        $selectLivre->execute();
            while ($unLivre = $selectLivre->fetch(PDO::FETCH_OBJ)) {
                echo"<div class='listeLivre'>";
                    echo "<ul>";
                        echo "<li>";
                        echo "Titre : " . $unLivre->titre ."<br> ISBN : " .$unLivre->isbn13 ."   " . "<a class='boutonSuprr' href='supprimer_livre.php?isbn13=<?php echo $unLivre->isbn13; ?> '><i class='fa-solid fa-trash'></i></a>";
                        echo "</li>";
                    echo "</ul>";
                    echo"</div>";
                }          
            }
            
        }   if (count($_SESSION['panier']) > 0) {
            echo "<form action=validerPanier.php>";
            echo "<br><button type='submit' class='btnValider'><i class='fa-solid fa-check'></i>Valider Le Panier <i class='fa-solid fa-check'></i></button>";
            echo "</form>";
            echo "<br>";
        }
    }else {
        echo "<p class='vide'>Votre panier est vide.</p>";
    }
}
    ?>
        </div>
        <?php
        if (isset($_SESSION['Mel'])) {
        ?>
            <div class="col-md-4 ">
                <h5>Connecté en tant que :</h5>
                <div class='form-group'>
                    <p>Mail: <?php echo $_SESSION['Mel']; ?></p>
                    <p>Nom: <?php echo $_SESSION['Nom']; ?></p>
                    <p>Prénom: <?php echo $_SESSION['Prenom']; ?></p>
                    <p>Adresse: <?php echo $_SESSION['Adresse']; ?></p>
                    <p>Ville: <?php echo $_SESSION['Ville']; ?></p>
                    <p>Code Postal: <?php echo $_SESSION['Codepostal']; ?></p>
                    <a href="utils/deconnexion.php" class="BoutonDeconnexion">
                        Déconnexion
                    </a>
                </div>

            </div>
        <?php
        } else {
        ?>
            <div class="col-md-4 ">
                <form action='identification.php' method='POST'>
                    <div class='form-group'>
                        <label for='Mel'>Identifiant</label>
                        <input type='text' class='form-control' id='Mel' name='Mel' required><br>
                        <label for='MotDePasse'>Mot de Passe</label>
                        <input type='password' class='form-control' id='MotDePasse' name='MotDePasse' required><br>
                        <button type='submit' class='btn btn-custom'>Se Connecter</button>
                    </div>
                </form>

            </div>

    </div>
    <br><br>
<?php
        }
        include "templates/footer.php";
?>