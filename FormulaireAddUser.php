<?php
session_start();
require_once("config/connexion.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "templates/headeradmin.php"; ?>
</head>
<style>
    body {
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        height: 100vh;
        margin: 0;
    }
    .AjoutUser {
        color: red;
        border: none;
        padding: 1rem;
        display: flex;
        flex-direction: column;
        align-items: center;
    }    
    
</style>

<body>
    <form action='AddUser.php' method='POST'>
        <div class='AjoutUser'>
            <h2>Créer un membre</h2>
            <div class="contenuajout">
                
                <label for="meluser"> Mel :</label>
                <input type='text' id='meluser' name='meluser' required><br><br>

                <label for="nom"> Nom :</label>
                <input type='text' id='nom' name='nom' required><br><br>
                
                <label for="prenom"> Prenom :</label>
                <input type='text' id='prenom' name='prenom' required><br><br>
                
                <label for="adresse"> Adresse :</label>
                <input type='text' id='adresse' name='adresse' required><br><br>
                
                <label for="ville"> Ville :</label>
                <input type='text' id='ville' name='ville' required><br><br>

                <label for="codePostal"> Code Postal :</label>
                <input type='text' id='codePostal' name='codePostal' required><br><br>

                <label for="mdpUser"> Mot de Passe User :</label>
                <input type='text' id='mdpUser' name='mdpUser' required><br><br>

                <button type='submit' class='btn btn-custom'>Valider</button>
          </div>
        </div>
    </form>
</body>