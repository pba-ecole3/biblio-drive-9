<?php
session_start();
if (isset($_SESSION['Profil'])) {
    if ($_SESSION['Profil'] == 'admin'){
      header('Location: accueilAdmin.php');
      exit();
    }
  }  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "templates/header.php"; ?>
</head>
<style>
    body {
        width: 100%;
    }

    .redirectionConnection {
        color: #fff;
        height: auto;
        width: 100%;
        border: 1px solid #ffffff;
        padding: 2rem;
        position: relative;
        margin: 0 auto;
        /* centrer le bloc */
        text-align: center;
        /* centrer le texte */
    }

    .infoUser {
        border: 1px solid #ffffff;
        color: #fff;
        text-align: center;
        padding-bottom: 20px;
    }

    .BoutonDeconnexion {

        text-align: center;
        color: #fff;
        /* couleur du texte */
        background-color: #06283d;
        /* couleur du fond */
        border-color: #ffffff;
        /* couleur de la bordure */
        border-radius: 10px;
        padding: 10px;
        text-decoration: none;
        /* Supprime le soulignement */
        color: #fff;
        /* Couleur du texte */
    }

    .BoutonDeconnexion:hover {
        background-color: orange;
        border-color: #06283d;
        color: #fff;
    }

    h1 {
        text-align: center;
    }
</style>

<body>


    <?php
    if (isset($_SESSION['Mel'])) {
    ?>
        <h1> Connexion réussie !</h1>

        <div class="infoUser">
            <h2>Vous êtes connecté en tant que :</h2>
            <div class="content">
                <p>Mail: <?php echo $_SESSION['Mel']; ?></p>
                <p>Nom: <?php echo $_SESSION['Nom']; ?></p>
                <p>Prénom: <?php echo $_SESSION['Prenom']; ?></p>
                <p>Adresse: <?php echo $_SESSION['Adresse']; ?></p>
                <p>Ville: <?php echo $_SESSION['Ville']; ?></p>
                <p>Code Postal: <?php echo $_SESSION['Codepostal']; ?></p>
            </div>

            <a href="utils/deconnexion.php" class="BoutonDeconnexion">
                Déconnexion
            </a>
        </div>
    <?php

    } else { // on a pas trouvé de ligne correspondant au mel et mot de passe
    ?>
        <div class='redirectionConnection'>
            <h2>Identifiant/Mot de Passe incorrect</h2>
            <form action='loginFormulaire.php' method='POST'>
                <button type='submit' class='btn btn-custom'>Se Connecter</button>
            </form>
        </div>
    <?php
    }
    ?>

</body>
<br><br><br><br>
<?php include "templates/footer.php"; ?>

</html>