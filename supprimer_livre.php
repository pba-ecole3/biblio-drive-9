<?php
session_start();

// Vérifier si l'ISBN13 est passé en paramètre
if (isset($_GET['isbn13'])) {
    $isbn13 = $_GET['isbn13'];

    // Vérifier si le panier existe dans la session
    if (isset($_SESSION['panier']) && !empty($_SESSION['panier'])) {
        // Parcourir le panier et supprimer le livre avec le bon ISBN13
        foreach ($_SESSION['panier'] as $index => $livre) {
            unset($_SESSION['panier'][$index]);
            break;
        }
    }
}

header("Location: panier.php");
exit();
