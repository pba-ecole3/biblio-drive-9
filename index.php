<html lang="en">
<?php session_start(); 
if (isset($_SESSION['Profil'])) {
  if ($_SESSION['Profil'] == 'admin'){
    header('Location: accueilAdmin.php');
    exit();
  }
}
?>

<head>
  <?php include "templates/header.php"; ?>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<style>
  body {
    overflow-x: hidden;
  }

  .BoutonDeconnexion {

    text-align: center;
    color: #fff;
    /* couleur du texte */
    background-color: #06283d;
    /* couleur du fond */
    border-color: #ffffff;
    /* couleur de la bordure */
    border-radius: 10px;
    padding: 10px;
    text-decoration: none;
    /* Supprime le soulignement */
    color: #fff;
    /* Couleur du texte */
  }

  .BoutonDeconnexion:hover {
    background-color: orange;
    border-color: #06283d;
    color: #fff;
  }
</style>
<?php

require_once("config/connexion.php");

$chercheAjout = "SELECT *
  FROM livre 
  ORDER BY dateajout DESC LIMIT 3;";
$select = $connexion->query($chercheAjout);

$select->setFetchMode(PDO::FETCH_OBJ);

?>

<body>
  <div class="row">
    <!-- Carousel -->
    <div class="col-md-8 col">

      <h2>Dernières acquisitions :</h2><br>
      <div id="Carousel" class="carousel slide" data-bs-ride="carousel" data-bs-interval="2000">
        <div class="carousel-inner">
          <?php $firstTour = true; ?>

          <?php while ($infoImages = $select->fetch()) : ?>
            <div class="carousel-item <?php if ($firstTour) : echo ' active';
                                        $firstTour = false;
                                      endif; ?>">
              <a href="livredetail.php?nolivre=<?php echo $infoImages->nolivre; ?>">
                <img class="d-block mx-auto" style="width: 30%;" src="<?php echo $infoImages->image; ?>" alt="Slide ">
              </a>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
      <br><br>
    </div>
    <br><br>

    <!-- FIN Carousel  -->

  


    <!-- Formulaire de connexion -->
    
    <br><br><br><br><br>
    <?php
    if (isset($_SESSION['Mel'])) {
    ?>
      <div class="col-md-4 ">
        <h5>Connecté en tant que :</h5>
        <div class='form-group'>
          <p>Mail: <?php echo $_SESSION['Mel']; ?></p>
          <p>Nom: <?php echo $_SESSION['Nom']; ?></p>
          <p>Prénom: <?php echo $_SESSION['Prenom']; ?></p>
          <p>Adresse: <?php echo $_SESSION['Adresse']; ?></p>
          <p>Ville: <?php echo $_SESSION['Ville']; ?></p>
          <p>Code Postal: <?php echo $_SESSION['Codepostal']; ?></p>
          <p>Vous avez <?php echo $_SESSION['NbrLivresEmpruntes']; ?> emprunt(s) en cours</p>
          <a href="utils/deconnexion.php" class="BoutonDeconnexion">
            Déconnexion
          </a>
        </div>

      </div>
    <?php
    } else {
    ?>
      <div class="col-md-4 ">
        <form action='identification.php' method='POST'>
          <div class='form-group'>
            <label for='Mel'>Identifiant</label>
            <input type='text' class='form-control' id='Mel' name='Mel' required><br>
            <label for='MotDePasse'>Mot de Passe</label>
            <input type='password' class='form-control' id='MotDePasse' name='MotDePasse' required><br>
            <button type='submit' class='btn btn-custom'>Se Connecter</button>
          </div>
        </form>
      </div>
  </div>
<?php
    }
?>
<!-- FIN Case connection -->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<?php include "templates/footer.php"; ?>
</body>

</html>