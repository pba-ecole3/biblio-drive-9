<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://kit.fontawesome.com/2f1484c304.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/styletemplates.css">

<header>


    <!-- Barre de Navigation Fixe -->

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark full-width-navbar" style="background-color: #06283d;">
        <a class="navbar-brand mb-0 h1 d-flex align-items-center">
            <img class="d-inline-block align-top" style="padding-top: 20px;" src="images/logo.png" width="80" height="100" />
            <span style="padding-left: 30px;" class="align-middle">Bibliothèque des Cerisiers</span>
        </a>
        <button type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" class="navbar-toggler" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a href="index.php" class="nav-link active">
                        Home
                    </a>
                </li>
                <li>
                    <a href="panier.php" class="nav-link active">
                        Panier
                    </a>
                </li>
                <li>
                    <?php
                    if (isset($_SESSION['Profil'])) {
                    ?>
                        <a href="profil.php" class="nav-link active">
                            Profil
                        </a>
                    <?php
                    } else {
                    ?>
                        <a href="loginFormulaire.php" class="nav-link active">
                            Se Connecter
                        </a>
                    <?php
                    }
                    ?>

                </li>
            </ul>
        </div>
    </nav>
</header>

<div class="row">

    <!-- barre recherche livre -->

    <div class="col-md-8 col">
        <br><br><br><br><br><br><br>

        <form action="livreauteur.php" method="get">
            <input type="text" name="nomAuteur" class="form-control" placeholder="Rechercher dans le catalogue (Saisie du nom de l'auteur)"><br><br>
        </form>
    </div>
</div>

<!-- <?php echo $_SERVER["REQUEST_URI"]; ?> -->