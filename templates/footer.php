<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://kit.fontawesome.com/2f1484c304.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/style.css">

<br><br>
<footer>
    <div class="footer-content col-md-12 d-flex justify-content-between align-items-center">
        <p class="identity"> Hamelin Victor</p>
        <p class="bts">BTS SIO </p>
    </div>
    <div class="footer-bottom col-md-12 text-center">
        <p class="site-name">&copy; <?php echo date('Y'); ?> Mon Site Web. Tous droits réservés.</p>
    </div>
</footer>