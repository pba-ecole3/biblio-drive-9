<?php
// Connexion au serveur BDD
try {
    $dns = "mysql:host=localhost;dbname=BiblioDrive"; //dbname = nom de la base
    $utilisateur = "root";
    $motDePasse = "";

    $connexion = new PDO($dns, $utilisateur, $motDePasse); //crée une nouvelle instance de la classe PDO → classe intégrée à PHP qui fournit une interface pour parler avec mysql
} catch (Exception $e) {
    echo "Connexion à MySQL impossible : <br>", $e->getMessage();
    die();
}
