<?php
require_once("config/connexion.php");
session_start();


$auteur = $_POST["auteur"];
$titre = $_POST["titre"];
$ISBN13 = $_POST["ISBN13"];
$AnneedeParution = $_POST["AnneedeParution"];
$resume = $_POST["resume"];
$imageAdd = $_POST["imageAdd"];
$dateAjout = date('Y-m-d');

$requete = "INSERT INTO `livre` (`nolivre`, `noauteur`, `titre`, `isbn13`, `anneeparution`, `resume`, `dateajout`, `image`) VALUES (NULL, :auteur, :titre, :isbn13, :anneeparution, :resume, :dateAjout, :imageAdd)";

$select = $connexion->prepare($requete);


$select->bindParam(':auteur', $auteur);
$select->bindParam(':titre', $titre);
$select->bindParam(':isbn13', $ISBN13);
$select->bindParam(':anneeparution', $AnneedeParution);
$select->bindParam(':resume', $resume);
$select->bindParam(':imageAdd', $imageAdd);
$select->bindParam(':dateAjout', $dateAjout);

if ($select->execute()) {
    header('Location: accueilAdmin.php');
} else {
    echo "Erreur lors de l'ajout";
}

?>