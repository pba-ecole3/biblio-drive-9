<?php
session_start();
if (isset($_SESSION['Profil'])) {
    if ($_SESSION['Profil'] == 'admin'){
      header('Location: accueilAdmin.php');
      exit();
    }
  }  
?>
<!DOCTYPE html>
<html>


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "templates/header.php"; ?>
</head>

<style>
    .content {
        text-align: justify;
        color: #fff;
        display: flex;
        flex-direction: column;
        align-items: center;
        border: 1px solid #ffffff;
        padding: 2rem;
    }

    * {
        color: #fff;
        overflow-x: hidden;
    }

    .identificationConnection {
        color: #fff;
        height: auto;
        width: 50%;
        border: 1px solid #ffffff;
        padding: 2rem;
        padding-top: 2rem;
        position: relative;
        margin: 0 auto;
        /*  centrer le bloc */
        text-align: center;
        /* centrer le texte */
    }

    input[type="text"],
    input[type="password"] {
        padding: 0.5rem;
        border-radius: 20px;
        border: 1px solid #fff;
        font-size: 1rem;
    }

    .error-message {
        color: red;
        text-align: center;
    }
</style>
<?php

if (isset($_SESSION['Mel'])) {
    header('Location: profil.php');
    exit();
} else {
?>

    <body>
        <div class='identificationConnection'>
            <h2> Identification </h2>
            <form action='identification.php' method='POST'>
                <div class='form-group'>
                    <label for='Mel'>Identifiant</label>
                    <input type='text' class='form-control' id='Mel' name='Mel' required><br>
                    <label for='MotDePasse'>Mot de Passe</label>
                    <input type='password' class='form-control' id='MotDePasse' name='MotDePasse' required><br>
                    <button type='submit' class='btn btn-custom'>Se Connecter</button>
                </div>
            </form>
        </div>
    </body>
<?php
}
?>

<?php include "templates/footer.php"; ?>

</html>